import cv2
import requests
import json

import utils

addr = 'http://localhost:5000'
url = addr + '/api/predict'

# prepare headers for http rchreequest
content_type = 'image/jpeg'
headers = {'content-type': content_type}

def predict(device):
    print('Start capture')
    capture_webcam = cv2.VideoCapture(device)

    img_captured = utils.get_frame(capture_webcam)

    cv2.imwrite('to_recogning.jpg', img_captured)

    to_recogning = cv2.imread('to_recogning.jpg')

    # encode image as jpeg
    _, img_encoded = cv2.imencode('.jpg', to_recogning)

    # send http request with image and receive response
    response = requests.post(url, data=img_encoded.tostring(), headers=headers)

    # decode response
    print(json.loads(response.text))


def main(device):
    predict(device)


if __name__ == '__main__':
    device = 0
    main(device)
