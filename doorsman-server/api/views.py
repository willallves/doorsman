from flask import request
import json
import cv2
import numpy as np

from api import utils, app

'''
Client post request with a image this url
server response with facenames recogning on the image
'''
@app.route("/api/predict", methods=['POST'])
def predict():
    names_recognings = []
    r = request

    nparr = np.fromstring(r.data, np.uint8)
    received_img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    cv2.imwrite('to_recogning.jpg', received_img)

    predictions = utils.predict("to_recogning.jpg", model_path="trained_model.clf")

    if not predictions:
        names_recognings = ["ERROR", "ERROR"]
    else:
        for name, (top, right, bottom, left) in predictions:
            print("- Found {} at ({}, {})".format(name, left, top))
            names_recognings.append(name)

    response = app.response_class(
        response=json.dumps(names_recognings),
        status=200,
        mimetype='application/json'
    )

    return response
